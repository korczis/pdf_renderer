# PDF Renderer

## Assignment

Create a service that accepts PDF files containing one or more pages. 
These pages should be rendered to “normalized png” files: they should fit into a 1200x1600 pixels rectangle.

The service is accessible through a REST API and offloads all file processing to 
asynchronous tasks (e.g. using dramatiq library), so that it is easy to scale.

### REST API

#### POST /documents

- uploads a file
- returns JSON { “id”: “<DOCUMENT_ID>? }

#### GET /documents/<DOCUMENT_ID>

- returns JSON { “status”: “processing|done”, “n_pages”: NUMBER }

#### GET /documents/<DOCUMENT_ID>/pages/<NUMBER>
   
- return rendered image png

### Implementation

- Python 3, Django (preferably) or Flask, possibly a database (e.g. PostgreSQL), message queue (e.g. rabbitmq), 
  we recommend dramatiq or a similar library.
- Should include a simple Dockerfile and docker-compose.yml to enable easy testing
  - Feel free to use [template](https://drive.google.com/file/d/1v1v9G7PcT0-8JoN2saIIcMTMmZ8hOo7P/view)
  - Take into account best practices in software development, such as proper code structure and tests. 
    However, for the purposes of this interview, a comprehensive demonstration isn't necessary. 
    It's enough to show your understanding of how and what to test, adding a comment that additional tests would 
    follow a similar approach.

### Instructions

- The aim of the task is to demonstrate your software engineering skills and experience
  - API should follow common practices, e.g. return proper status codes, content-type, etc.
- We will check the solution and do a “code review” that we will go through on a follow-up call.
- You have up to a week for the implementation, but it should not take more than several hours to complete.
- If anything from the assignment is not clear, please let us know
- You can send us a zip or a link to a git repository.
- Include a short README.md that describes how to build the service and how to run an example conversion using e.g. curl or wget.

## Getting started

### Required services

Ensure PostgreSQL and RabbitMQ are running.

### Install prerequisites

Install necessary dependencies using the provided script.

```bash
apt-get update && \
apt-get install -y --no-install-recommends \
    libtiff5-dev \
    libjpeg62-turbo-dev \
    libpng-dev\
    zlib1g-dev \
    libfreetype6-dev \
    liblcms2-dev \
    libwebp-dev \
    tcl-dev \
    tk-dev \
    libharfbuzz-dev \
    libfribidi-dev \
    libxcb1-dev \
    poppler-utils
```

### Create virtual environment

Set up a virtual environment using Python 3.

```bash
$  python3 -m venv .venv    
```

### Activate virtual environment

```bash
$ source .venv/bin/activate
```

### Install dependencies

Use Poetry to install project dependencies.

```bash
$ poetry install
```

### Initialize database

Create a PostgreSQL database named "pdf_renderer".

```bash
$ psql -h localhost -U postgres -c "CREATE DATABASE pdf_renderer;"
```

### Run migrations

Apply database migrations to set up the schema.

```bash
$ python manage.py makemigrations
$ python manage.py migrate
```

### Run tests

Execute tests to ensure everything is working as expected.

```bash
$ python manage.py test
```

### Start dramatiq broker

Run the Dramatiq broker to handle asynchronous tasks.

```bash
DJANGO_SETTINGS_MODULE=pdf_renderer.settings dramatiq documents.tasks
```

### Start server

Start the Django server to begin handling API requests.

```bash
$ python manage.py startapp documents
```

Then visit [http://localhost:8000](http://localhost:8000) in your favorite web browser.

## Docker

### Build docker image

Build the Docker image for the PDF Renderer service.

```bash
$ docker build -t pdf_renderer .
```

### Run docker image 

Run the Docker image to deploy the PDF Renderer service in a container.

```bash
$ docker run -p 8000:8000 pdf_renderer
```

## Docker Compose stack

### Start stack

Use Docker Compose to start the entire stack, including PostgreSQL, RabbitMQ, and the PDF Renderer service.

```bash
$ docker-compose up --build --remove-orphans
```

### Teardown stack

Shutdown and remove the Docker Compose stack.

```bash
$ docker-compose down
```

## Conclusion

With PDF Renderer, you can easily upload PDF files and retrieve rendered pages 
through a simple and efficient RESTful API. Follow the provided setup instructions to get started 
with the service and enjoy seamless PDF rendering capabilities.