# Use a base image with Python installed
FROM python:3.11-slim

# Set environment variables
ENV VIRTUAL_ENV=/opt/venv
ENV PATH="$VIRTUAL_ENV/bin:$PATH"

ENV RABBITMQ_URL="amqp://guest:guest@rabbitmq:5672/"

# Install system dependencies
RUN apt-get update && \
    apt-get install -y --no-install-recommends \
    libtiff5-dev \
    libjpeg62-turbo-dev \
    libpng-dev\
    zlib1g-dev \
    libfreetype6-dev \
    liblcms2-dev \
    libwebp-dev \
    tcl-dev \
    tk-dev \
    libharfbuzz-dev \
    libfribidi-dev \
    libxcb1-dev \
    poppler-utils

# Set the working directory in the container
WORKDIR /app

# Copy the project files to the container
COPY . .

# Create a virtual environment and activate it
RUN python -m venv $VIRTUAL_ENV
RUN echo "source $VIRTUAL_ENV/bin/activate" > /etc/profile.d/venv.sh
RUN chmod +x /etc/profile.d/venv.sh
RUN /bin/bash -c "source /etc/profile.d/venv.sh"

# Install project dependencies
RUN pip install --no-cache-dir poetry
RUN poetry config virtualenvs.create false
RUN poetry install --only main

EXPOSE 8000

# Command to run the application
CMD ["python", "manage.py", "runserver", "0.0.0.0:8000"]
