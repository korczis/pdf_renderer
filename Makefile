# PDF Renderer Makefile

# Format code using Black
format:
	python -m black .

# Install project dependencies
deps:
	poetry install

# Apply database migrations
migrate:
	python manage.py makemigrations
	python manage.py migrate

# Run tests
test:
	python manage.py test

# Run the Django server
run:
	python manage.py runserver

# Build Docker image for PDF Renderer service
image-build:
	docker build -t pdf_renderer .

# Start Docker Compose stack
stack-up:
	docker-compose up --build --remove-orphans

# Stop and remove Docker Compose stack
stack-down:
	docker-compose down