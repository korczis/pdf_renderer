"""
serializers.py - DocumentUploadSerializer, DocumentSerializer, PageSerializer

This module contains serializers for the Document and Page models.

Classes:
    DocumentUploadSerializer: Serializer for uploading documents.
    DocumentSerializer: Serializer for Document model.
    PageSerializer: Serializer for Page model.

Usage Example:
    In the 'serializers.py' file of the 'documents' app:
        class DocumentUploadSerializer(serializers.ModelSerializer):
            ...

        class DocumentSerializer(serializers.ModelSerializer):
            ...

        class PageSerializer(serializers.ModelSerializer):
            ...
"""

from rest_framework import serializers
from .models import Document, Page


class DocumentUploadSerializer(serializers.ModelSerializer):
    """
    Serializer for uploading documents.

    Attributes:
        model (Document): Document model to serialize.
        fields (list): Fields to include in the serializer.
    """

    class Meta:
        model = Document
        fields = ["file"]

    def create(self, validated_data):
        """
        Creates a new Document instance.

        Args:
            validated_data (dict): Validated data for creating the document.

        Returns:
            Document: Newly created Document instance.
        """
        return Document.objects.create(upload=validated_data["file"])


class DocumentSerializer(serializers.ModelSerializer):
    """
    Serializer for Document model.

    Attributes:
        model (Document): Document model to serialize.
        fields (list): Fields to include in the serializer.
    """

    class Meta:
        model = Document
        fields = "__all__"


class PageSerializer(serializers.ModelSerializer):
    """
    Serializer for Page model.

    Attributes:
        model (Page): Page model to serialize.
        fields (tuple): Fields to include in the serializer.
    """

    class Meta:
        model = Page
        fields = ("page_number", "image")
