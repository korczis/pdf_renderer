"""
models.py - Document and Page Models

This module contains the Document and Page models, which represent uploaded documents and their pages.

Classes:
    Document: Model representing an uploaded document.
    Page: Model representing a page of a document.

Attributes:
    Document.upload: A FileField representing the uploaded document file.
    Document.status: A CharField representing the status of the document processing.
    Page.document: A ForeignKey field representing the parent Document object.
    Page.page_number: An IntegerField representing the page number.
    Page.image: An ImageField representing the image of the page.

Methods:
    Document.number_of_pages(): Returns the number of pages associated with the document.

Relationships:
    Document.pages: One-to-Many relationship with Page model, representing the pages of the document.

Usage Example:
    document = Document.objects.create(upload=my_file, status="processing")
    page = Page.objects.create(document=document, page_number=1, image=my_image)

"""

from django.db import models


class Document(models.Model):
    """
    Model representing an uploaded document.

    Attributes:
        upload (FileField): Represents the uploaded document file.
        status (CharField): Represents the status of the document processing.

    Methods:
        number_of_pages(): Returns the number of pages associated with the document.
    """

    upload = models.FileField(upload_to="uploads/")
    status = models.CharField(max_length=20, default="processing")

    def number_of_pages(self):
        """
        Returns the number of pages associated with the document.

        Returns:
            int: The number of pages.
        """

        return self.pages.count()


class Page(models.Model):
    """
    Model representing a page of a document.

    Attributes:
        document (ForeignKey): Represents the parent Document object.
        page_number (IntegerField): Represents the page number.
        image (ImageField): Represents the image of the page.
    """

    document = models.ForeignKey(
        Document, related_name="pages", on_delete=models.CASCADE
    )
    page_number = models.IntegerField()
    image = models.ImageField(upload_to="images/")
