"""
views.py - Document Views

This module contains views for handling document-related operations, including uploading documents, checking document status, retrieving pages, and rendering the home page.

Classes/Functions:
    upload_document: View for uploading a PDF document for rendering.
    document_status: View for checking the status of a document.
    get_page: View for retrieving a specific page of a document.
    home_page_view: View for rendering the home page.
"""

import os

from django.shortcuts import render
from django.http import HttpResponse

# documents/views.py
from rest_framework import status
from rest_framework.response import Response
from rest_framework.decorators import api_view

from pdf_renderer import settings
from .models import Document, Page
from .serializers import DocumentUploadSerializer, DocumentSerializer, PageSerializer
import pdf2image
from drf_yasg.utils import swagger_auto_schema
from drf_yasg import openapi

from .tasks import process_document


@swagger_auto_schema(
    method="post",
    operation_summary="Uploads a PDF document for rendering.",
    operation_description="Uploads a PDF document to the server for rendering. The server saves the uploaded file and creates a Document instance in the database. It then triggers the `process_document` task to render the document's pages asynchronously.",
    request_body=openapi.Schema(
        type=openapi.TYPE_OBJECT,
        required=["file"],
        properties={"file": openapi.Schema(type=openapi.TYPE_FILE)},
    ),
    responses={201: openapi.Response(description="Created"), 400: "Bad Request"},
)
@api_view(["POST"])
def upload_document(request):
    """
    Uploads a PDF document for rendering.

    Workflow:
    1. The client sends a POST request with the PDF file to be uploaded.
    2. The server receives the request and saves the uploaded file to the specified location.
    3. The server creates a Document instance in the database with the uploaded file.
    4. The server triggers the process_document task to render the document's pages asynchronously.
    5. The server returns a JSON response containing the details of the uploaded document.

    Example curl command:
    curl -X POST -F "file=@data/django-readthedocs-io-en-5.0.x.pdf" http://localhost:8000/documents/
    """

    file = request.data.get("file")
    if not file:
        return Response(
            {"error": "File not provided"}, status=status.HTTP_400_BAD_REQUEST
        )

    # Save the uploaded file to the upload path
    file_path = os.path.join(settings.UPLOADS_ROOT, file.name)
    with open(file_path, "wb") as destination:
        for chunk in file.chunks():
            destination.write(chunk)

    document = Document.objects.create(upload=file)
    # render_pages.delay(document.id)
    serializer = DocumentSerializer(document)

    # process_document(document.id)
    process_document.send(document.id)

    return Response(serializer.data, status=status.HTTP_201_CREATED)


@swagger_auto_schema(
    method="get",
    operation_summary="Retrieve the status of a document.",
    operation_description="Retrieve the status of the specified document from the database and return the document's status along with the number of pages.",
    responses={
        200: openapi.Response(description="Document status and number of pages"),
        404: "Document not found",
    },
)
@api_view(["GET"])
def document_status(request, document_id):
    """
    Retrieves the status of a document.

    Workflow:
    1. The client sends a GET request with the document ID.
    2. The server retrieves the document from the database.
    3. The server returns a JSON response containing the document's status and the number of pages.

    Args:
        document_id (int): The ID of the document.

    Returns:
        Response: A JSON response containing the document status.

    Raises:
        Document.DoesNotExist: If the specified document does not exist.

    Example curl command:
    curl -X GET http://localhost:8000/documents/1/
    """
    try:
        document = Document.objects.get(id=document_id)
        return Response(
            {
                "id": document_id,
                "status": document.status,
                "n_pages": document.pages.count(),
            }
        )
    except Document.DoesNotExist:
        return Response(
            {"error": "Document not found"}, status=status.HTTP_404_NOT_FOUND
        )


@swagger_auto_schema(
    method="get",
    operation_summary="Retrieve a specific page of a document and return its image as PNG.",
    operation_description="Retrieve the specified page of a document from the database and return its image as a PNG file.",
    responses={
        200: openapi.Response(description="PNG image of the page"),
        404: "Page not found",
    },
)
@api_view(["GET"])
def get_page(request, document_id, page_number):
    """
    Retrieves a specific page of a document and returns its image as PNG.

    Workflow:
    1. The client sends a GET request with the document ID and page number.
    2. The server retrieves the specified page from the database.
    3. The server returns the image of the page as PNG.

    Args:
        document_id (int): The ID of the document.
        page_number (int): The page number.

    Returns:
        HttpResponse: The PNG image of the page.

    Raises:
        Page.DoesNotExist: If the specified page does not exist.
    """
    try:
        page = Page.objects.get(document_id=document_id, page_number=page_number)
        # Assuming page.image is the ImageField containing the page image
        image_data = page.image.read()
        return HttpResponse(image_data, content_type="image/png")
    except Page.DoesNotExist:
        return HttpResponse("Page not found", status=status.HTTP_404_NOT_FOUND)


def home_page_view(request):
    return HttpResponse("Hello, World!")
