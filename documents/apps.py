"""
apps.py - DocumentsConfig

This module contains the AppConfig subclass for the 'documents' app.

Classes:
    DocumentsConfig: AppConfig subclass for the 'documents' app.

Attributes:
    DocumentsConfig.default_auto_field: Default auto field for model definition.
    DocumentsConfig.name: Name of the app.

Usage Example:
    In the 'apps.py' file of the 'documents' app:
        class DocumentsConfig(AppConfig):
            default_auto_field = "django.db.models.BigAutoField"
            name = "documents"
"""

from django.apps import AppConfig


class DocumentsConfig(AppConfig):
    """
    AppConfig subclass for the 'documents' app.

    Attributes:
        default_auto_field (str): Default auto field for model definition.
        name (str): Name of the app.
    """

    default_auto_field = "django.db.models.BigAutoField"
    name = "documents"
