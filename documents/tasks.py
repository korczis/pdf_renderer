"""
tasks.py - Document Processing Tasks

This module contains tasks for processing documents using Dramatiq.

Classes:
    process_document: A Dramatiq actor for processing documents.

Dependencies:
    - io: Input/output operations.
    - pdf2image: Library for converting PDFs to images.
    - dramatiq: Library for background task processing.
    - django: Django framework for web development.
    - InMemoryUploadedFile: Class for handling uploaded files in Django.
    - apps: Module for interacting with Django applications.
    - RabbitmqBroker: RabbitMQ broker for messaging in Dramatiq.
"""

import io
import os

import pdf2image
import dramatiq
import django
from django.core.files.uploadedfile import InMemoryUploadedFile
from django.apps import apps

# Import the AMQP broker from dramatiq_amqp
from dramatiq.brokers.rabbitmq import RabbitmqBroker

# Initialize RabbitMQ broker
broker = RabbitmqBroker(
    url=os.environ.get("RABBITMQ_URL", "amqp://guest:guest@rabbitmq:5672/")
)

# Register the broker with Dramatiq
dramatiq.set_broker(broker)

# Call django.setup() to initialize Django
django.setup()


@dramatiq.actor
def process_document(document_id):
    """
    Process a document by converting it into images and saving the pages.

    Args:
        document_id (int): The ID of the document to process.

    Raises:
        Document.DoesNotExist: If the specified document does not exist in the database.

    Workflow:
        1. Retrieve the Document object from the database based on the provided ID.
        2. Extract the path to the uploaded PDF file from the Document object.
        3. Convert the PDF file into a sequence of images using pdf2image.
        4. Iterate over the images and create a Page object for each one.
        5. Save each image as a PNG file in an InMemoryUploadedFile.
        6. Save the Page object to the database.
        7. Update the status of the Document to 'done'.
    """

    try:
        # Import Django models inside the function to ensure Django is fully initialized
        Document = apps.get_model("documents", "Document")
        Page = apps.get_model("documents", "Page")

        document = Document.objects.get(id=document_id)
        pdf_path = document.upload.path
        images = pdf2image.convert_from_path(pdf_path, dpi=300, size=(1200, 1600))

        for i, image in enumerate(images):
            page = Page(document=document, page_number=i + 1)

            # Convert the PIL image to bytes
            img_byte_array = io.BytesIO()
            image.save(img_byte_array, format="PNG")
            img_byte_array.seek(0)

            # Create an InMemoryUploadedFile from the bytes
            page.image.save(
                f"page_{i + 1}.png",
                InMemoryUploadedFile(
                    img_byte_array,
                    None,
                    f"page_{i + 1}.png",
                    "image/png",
                    img_byte_array.tell(),
                    None,
                ),
            )

            page.save()

        document.status = "done"
        document.save()
    except Document.DoesNotExist:
        pass
