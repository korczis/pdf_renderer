from django.urls import path

from . import views

urlpatterns = [
    path("", views.upload_document, name="upload_document"),
    path("<int:document_id>/", views.document_status, name="document_status"),
    path(
        "<int:document_id>/pages/<int:page_number>/",
        views.get_page,
        name="get_page",
    ),
]
