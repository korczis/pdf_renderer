"""
Document API Test

This test suite contains tests for the Document API endpoints, ensuring the correct behavior of document-related operations such as uploading documents and retrieving document status and pages.

1. Upload Document Test:
    - Validates the behavior of the API endpoint responsible for uploading PDF documents.
    - Workflow:
        1. The client sends a POST request with a sample PDF file.
        2. The server receives the file and creates a new Document object in the database.
        3. The server responds with a success status code and the status of the document set to "processing".

2. Get Document Status Test:
    - Verifies the functionality of retrieving the status of a document from the server.
    - Workflow:
        1. The client sends a GET request with the document ID.
        2. The server retrieves the document from the database.
        3. The server responds with a success status code and the status of the document set to "done".

3. Get Page Test:
    - Checks the endpoint for retrieving a specific page of a document along with its image.
    - Workflow:
        1. The test creates a dummy document with a single page containing a dummy image.
        2. The client sends a GET request to retrieve the page image.
        3. The server responds with a success status code and the image data in PNG format.

Example Usage:
    - Ensure that the sample PDF file (data/sample.pdf) exists in the specified location for the upload document test.
    - Adjust the test data and assertions as necessary based on the application requirements and data models.
"""

from django.core.files.uploadedfile import SimpleUploadedFile
from django.test import TestCase
from rest_framework.test import APIClient
from rest_framework import status
from .models import Document


class DocumentAPITest(TestCase):
    """
    Test suite for Document API endpoints.
    """

    def setUp(self):
        """
        Setup method to initialize the test client.
        """
        self.client = APIClient()

    def test_upload_document(self):
        """
        Test case for uploading a document.
        """

        # Create a test PDF file
        test_pdf = open("data/sample.pdf", "rb")
        response = self.client.post(
            "/documents/", {"file": test_pdf}, format="multipart"
        )
        self.assertEqual(response.status_code, status.HTTP_201_CREATED)
        self.assertEqual(Document.objects.count(), 1)
        self.assertEqual(response.data["status"], "processing")

    def test_get_document_status(self):
        """
        Test case for retrieving document status.
        """

        # Create a test document
        document = Document.objects.create()
        response = self.client.get(f"/documents/{document.id}/")
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(response.data["status"], "done")

    def test_get_page(self):
        """
        Test case for retrieving a specific page of a document.
        """

        # Create a test document with a page
        document = Document.objects.create()
        # Create a dummy image file
        image_data = b"dummy_image_data"
        image_file = SimpleUploadedFile(
            "page_image.png", image_data, content_type="image/png"
        )
        page = document.pages.create(page_number=1, image=image_file)

        # Make a GET request to the get_page endpoint
        response = self.client.get(
            f"/documents/{document.id}/pages/{page.page_number}/"
        )

        # Check if the response status code is 200 OK
        self.assertEqual(response.status_code, status.HTTP_200_OK)

        # Check if the response content type is image/png
        self.assertEqual(response.headers["Content-Type"], "image/png")
        # Check if the response content matches the dummy image data
        self.assertEqual(response.content, image_data)
